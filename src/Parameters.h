#ifndef __Parameters_H__
#define __Parameters_H__

// If CAM is 0, the webcam is not used at all, replaced by keyboard controls
#define CAM 1

// Disables off-axis projection, the sled is still controlled by head tracking
#define OFF_AXIS_PROJECTION 1

// Disables brake tracking, replaced by keyboard controls (I/K)
#define TRACK_BRAKE 1

// Tracks the QR code (0), or tracks the fist (1)
#define QR 1

// Enables ArUCo
#define ARUCO 0

#endif
