#ifndef __Tracker_H__
#define __Tracker_H__

#include "cv.h"
#include "highgui.h"

#include <Ogre.h>

#include <iostream>
#include <cstdio>

class Tracker {

public:
	Tracker();
	~Tracker();
	void update();
	int posXHead, posYHead, posZHead, posXBrake, posYBrake;


private:
	void detectFace(cv::Mat&, cv::CascadeClassifier&, double);
	void detectBrake(cv::Mat&, cv::CascadeClassifier&, double);
	cv::String cascadeNameHead;
	cv::String cascadeNameBrake;
	cv::CascadeClassifier cascadeHead;
	cv::CascadeClassifier cascadeBrake;
	double scale;

	cv::VideoCapture cvCapture;
	cv::Mat img;

};

#endif

