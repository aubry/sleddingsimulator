#ifndef __SleddingSimulator_H__
#define __SleddingSimulator_H__

#define PHYSICS 0

#include <Ogre.h>
#include "InputListener.h"
#include "Sled.h"
#include "Terrain.h"

#include "OgreBulletDynamicsRigidBody.h"

#include "OgreBulletCollisionsShape.h"

 
#include "Shapes/OgreBulletCollisionsBoxShape.h"
#include "Shapes/OgreBulletCollisionsSphereShape.h"
#include "Shapes/OgreBulletCollisionsConeShape.h"
#include "Shapes/OgreBulletCollisionsCylinderShape.h"
#include "Shapes/OgreBulletCollisionsTriangleShape.h"
#include "Shapes/OgreBulletCollisionsStaticPlaneShape.h"

#include "Shapes/OgreBulletCollisionsCompoundShape.h"

#include "Shapes/OgreBulletCollisionsMultiSphereShape.h"

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsMinkowskiSumShape.h"

#include "Shapes/OgreBulletCollisionsTrimeshShape.h"

#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"

#include "OgreBulletCollisionsRay.h"

#include "Debug/OgreBulletCollisionsDebugLines.h"

#include "OgreBulletDynamicsWorld.h"
#include "OgreBulletDynamicsRigidBody.h"

#include "OgreBulletDynamicsConstraint.h"
#include "Constraints/OgreBulletDynamicsPoint2pointConstraint.h" 

#include <OgreCompositorManager.h>


class SleddingSimulator
{

public:
	SleddingSimulator();
	~SleddingSimulator();
 
	bool start();
	void loadRessources();
	void setUpScene();
	void createScene();
	void setUpPhysic();
	void createFrameListener();

	OgreBulletDynamics::RigidBody* addStaticPlane(const Ogre::Real bodyRestitution, const Ogre::Real bodyFriction);

private:
	
	Ogre::Root* mRoot;
	Ogre::RenderWindow* mWindow;
	Ogre::SceneManager* mSceneMgr;
	Ogre::Camera* mCamera;

	OgreBulletDynamics::DynamicsWorld* mWorld;
	OgreBulletCollisions::DebugDrawer* debugDrawer;
	int mNumEntitiesInstanced;
	std::deque<OgreBulletDynamics::RigidBody*> mBodies;
	std::deque<OgreBulletCollisions::CollisionShape*> mShapes;

	InputListener* mInputListener;

	Sled* mSled;
	Terrain* mTerrain;
	
};

#endif
