#ifndef __InputFrameListener_H__
#define __InputFrameListener_H__

#include <Ogre.h>

#include <OIS.h>

#include "Sled.h"

#include "Parameters.h"

#if ARUCO==1
#include "TrackerAruco.h"
#else
#include "Tracker.h"
#endif

#include "OgreBulletDynamicsRigidBody.h"
 
class InputListener : public Ogre::FrameListener, public Ogre::WindowEventListener
{
public:

	InputListener(Ogre::RenderWindow* win, Ogre::Camera* cam, OgreBulletDynamics::DynamicsWorld* wrld, Sled* sled, bool bufferedKeys = false, bool bufferedMouse = false, bool bufferedJoy = false);
	~InputListener();

	bool frameRenderingQueued(const Ogre::FrameEvent& evt);
	bool processUnbufferedKeyInput(const Ogre::FrameEvent& evt);
	bool processUnbufferedMouseInput(const Ogre::FrameEvent& evt);
	bool frameStarted(const Ogre::FrameEvent& evt);
	bool frameEnded(const Ogre::FrameEvent& evt);
	void moveCamera();
	void windowResized(Ogre::RenderWindow* rw);
	void windowClosed(Ogre::RenderWindow* rw);
	void showDebugOverlay(bool show);
	
protected:
	Ogre::Camera* mCamera;

	Sled* mSled;
	Ogre::Node* mBrakeNode;

	Ogre::Vector3 mTranslateVector;
	Ogre::Real mCurrentSpeed;
	Ogre::RenderWindow* mWindow;
	OgreBulletDynamics::DynamicsWorld* mWorld;
	
	float mMoveScale;
	float mSpeedLimit;
	Ogre::Degree mRotScale;

	Ogre::Real mTimeUntilNextToggle ;
	Ogre::Radian mRotX, mRotY;

	int mSceneDetailIndex ;
	Ogre::Real mMoveSpeed;
	Ogre::Degree mRotateSpeed;
 
	OIS::InputManager*  mInputManager;
	OIS::Mouse*         mMouse;
	OIS::Keyboard*      mKeyboard;
	OIS::JoyStick*      mJoy;

	Ogre::Overlay* mDebugOverlay;

	Ogre::Vector3 brakePos;
	Ogre::Vector3 brakeDelta;
	Ogre::Vector3 lastBrakePos;
	Ogre::Vector3 headPos; 
	Ogre::Vector3 headDelta;
	Ogre::Vector3 lastHeadPos;
#if ARUCO==1
	TrackerAruco* tracker;
#else
	Tracker* tracker;
#endif
	int headCount;
	Ogre::Matrix4 previousMatrix;

	bool motionBlurIsEnabled;
};

#endif
