#ifndef __TrackerAruco_H__
#define __TrackerAruco_H__

#include "cv.h"
#include "highgui.h"

#include <Ogre.h>
#include <aruco/aruco.h> 

#include <iostream>
#include <cstdio>

class TrackerAruco {

public:
	TrackerAruco(Ogre::Quaternion newOrientation);
	~TrackerAruco();
	void update();
	int posXHead, posYHead, posZHead, posXBrake, posYBrake;
	Ogre::Quaternion mNewOrientation;

private:
	void detectFace(cv::Mat&, cv::CascadeClassifier&, double);
	void detectMarkers (cv::Mat& img);
	cv::String cascadeNameHead;
	cv::CascadeClassifier cascadeHead;
	double scale;

	cv::VideoCapture cvCapture;
	cv::Mat img;

	// ArUCo part
	Ogre::Vector3 mInitialMarkerPosition;
	Ogre::Real mInitialAngle;

	aruco::MarkerDetector mDetector; // The detector of makers
	vector<aruco::Marker> mMarkers; // The set of markers detected in the image
	bool firstDetection;

	Ogre::Real bound(Ogre::Real currentPos, Ogre::Real initialPos);
};

#endif

