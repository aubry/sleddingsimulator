#ifndef __Terrain_H__
#define __Terrain_H__

#include "OgreBulletDynamics.h"
#include "OgreBulletDynamicsWorld.h"

class Terrain
{
public:
	Terrain(Ogre::SceneManager* sMgr, OgreBulletDynamics::DynamicsWorld* world, int numEn, Ogre::Real bodyRestitution, Ogre::Real bodyFriction);
	~Terrain();

	OgreBulletDynamics::RigidBody* createTerrain(const Ogre::String& instanceName,
												 const Ogre::String& meshName,
												 const Ogre::Vector3& pos, 
												 const Ogre::Quaternion& q, 
												 const Ogre::Real bodyRestitution, 
												 const Ogre::Real bodyFriction,
												 bool  castShadow);

	void addTree(Ogre::Entity* ent,Ogre::StaticGeometry* sg, const Ogre::Vector3& pos);
	void addTrees();

	/* data */
private:
	Ogre::SceneManager* mSceneMgr;
	int mNumEntitiesInstanced;
	OgreBulletDynamics::DynamicsWorld* mWorld;
	std::deque<OgreBulletDynamics::RigidBody*> mBodies;
	std::deque<OgreBulletCollisions::CollisionShape*> mShapes;
};

#endif