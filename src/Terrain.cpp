#include "Terrain.h"

#include "OgreBulletCollisionsShape.h"

#include "Shapes/OgreBulletCollisionsConeShape.h"

#include "Shapes/OgreBulletCollisionsTrimeshShape.h"

#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"

#include "OgreBulletDynamicsWorld.h"
#include "OgreBulletDynamicsRigidBody.h"

#include "OgreBulletDynamicsConstraint.h"
#include "Constraints/OgreBulletDynamicsPoint2pointConstraint.h" 

Terrain::Terrain(Ogre::SceneManager* sMgr, OgreBulletDynamics::DynamicsWorld* world, int numEnt, Ogre::Real bodyRestitution, Ogre::Real bodyFriction)
{
	mSceneMgr = sMgr;
	mNumEntitiesInstanced = numEnt;
	mWorld = world;

	createTerrain("sceneMesh", "Grid.mesh", Ogre::Vector3(0,0,0), Ogre::Quaternion::IDENTITY, bodyRestitution, bodyFriction, false); 
	
	addTrees();

	sMgr->setSkyDome(true, "Sky", 5, 1);

	Ogre::ColourValue fadeColour(0.9, 0.9, 0.9);
	sMgr->setFog(Ogre::FOG_EXP, fadeColour, 0.005);
}

Terrain::~Terrain(){
	// OgreBullet physic delete - RigidBodies
	std::deque<OgreBulletDynamics::RigidBody*>::iterator itBody = mBodies.begin();
	while (mBodies.end() != itBody){   
		delete *itBody;
		++itBody;
	}   
	// OgreBullet physic delete - Shapes
	std::deque<OgreBulletCollisions::CollisionShape*>::iterator itShape = mShapes.begin();
	while (mShapes.end() != itShape){   
		delete *itShape;
		++itShape;
	}
	mBodies.clear();
	mShapes.clear();

}

OgreBulletDynamics::RigidBody* Terrain::createTerrain(	const Ogre::String& instanceName,
														const Ogre::String& meshName,
														const Ogre::Vector3& pos, 
														const Ogre::Quaternion& q, 
														const Ogre::Real bodyRestitution, 
														const Ogre::Real bodyFriction,
														bool castShadow)
{
	Ogre::Entity* sceneEntity = mSceneMgr->createEntity(instanceName + Ogre::StringConverter::toString(mNumEntitiesInstanced), meshName);
	sceneEntity->setCastShadows (castShadow);

	OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = new OgreBulletCollisions::StaticMeshToShapeConverter(sceneEntity);
	OgreBulletCollisions::TriangleMeshCollisionShape *sceneTriMeshShape = trimeshConverter->createTrimesh();
	delete trimeshConverter;
	OgreBulletDynamics::RigidBody* sceneRigid = new OgreBulletDynamics::RigidBody(instanceName + "Rigid" + Ogre::StringConverter::toString(mNumEntitiesInstanced), mWorld);

	Ogre::SceneNode *node = mSceneMgr->getRootSceneNode ()->createChildSceneNode ();
	node->attachObject (sceneEntity);

	sceneRigid->setStaticShape(node, sceneTriMeshShape, bodyRestitution, bodyFriction, pos);

	mShapes.push_back(sceneTriMeshShape);
	mBodies.push_back(sceneRigid);
	mNumEntitiesInstanced++;

	return sceneRigid;
}

void Terrain::addTree(Ogre::Entity* ent,Ogre::StaticGeometry* sg, const Ogre::Vector3& pos)
{
	sg->addEntity(ent, pos, Ogre::Quaternion(1,0,0,0), Ogre::Vector3(1,1,1));
	
	Ogre::Vector3 size = Ogre::Vector3::ZERO; 
	Ogre::AxisAlignedBox boundingB = ent->getBoundingBox();
	size = boundingB.getSize();
	size /= 2.0f; // only the half needed
	size *= 0.96f;
	// after that create the Bullet shape with the calculated size
	OgreBulletCollisions::ConeCollisionShape *sceneConeShape = new OgreBulletCollisions::ConeCollisionShape(size[0]*1.5, 2*(size[1]*1.25), Ogre::Vector3(0, 1,0));
	// and the Bullet rigid body
	OgreBulletDynamics::RigidBody* defaultBody = new OgreBulletDynamics::RigidBody("defaultConeRigid_", mWorld);
	defaultBody->setStaticShape(sceneConeShape,
								0.6f,  // dynamic body restitution
								0.6f,  // dynamic body friction
								pos,   // starting position of the box
								Ogre::Quaternion(1,0,0,0)); // orientation of the box
	mNumEntitiesInstanced++;
	         
	// push the created objects to the deques
	mShapes.push_back(sceneConeShape);
	mBodies.push_back(defaultBody);  
}

void Terrain::addTrees()
{
	Ogre::Entity* ent;
	Ogre::SceneNode* node;
	ent = mSceneMgr->createEntity("tree", "Sapin.mesh");
	Ogre::StaticGeometry* sg = mSceneMgr->createStaticGeometry("sg");

	/*Create trees*/
	for (int i = 0; i < 130; ++i)
	{   int x = i*6+Ogre::Math::RangeRandom(-20.0f, 20.0f);
		int z = 0+Ogre::Math::RangeRandom(-105.0f, 85.0f);
		int y = -0.272*x-6;
		Ogre::Vector3 pos = Ogre::Vector3(x,y,z);
		addTree(ent, sg, pos);
	}

	for (int i = 0; i < 17; ++i)
	{   int x = 750;
		int z = i*5;
		int y = -0.272*x - 7;
		Ogre::Vector3 pos = Ogre::Vector3(x,y,z);
		addTree(ent, sg, pos);
		Ogre::Vector3 pos_ = Ogre::Vector3(x,y,-z);
		addTree(ent, sg, pos_);
	}

	sg->build();
	mSceneMgr->destroyEntity(ent);
}
