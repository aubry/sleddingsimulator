#include "Tracker.h"
#include "Parameters.h"

#include <iostream>
#include <cstdio>

// constructor
Tracker::Tracker() {
	// set cascade xml
	cascadeNameHead = "./haar/haarcascade_frontalface_alt.xml";
#if QR==1
	cascadeNameBrake = "./haar/marker2.xml";
#else
	cascadeNameBrake = "./haar/handFistHAAR.xml";
#endif
	
	// position
	posXHead = 0;
	posYHead = 0;
	posZHead = 0;
	posXBrake = 0;
	posYBrake = 0;
	
	// scale
	scale = 1;
	// try to load the cascade
	cascadeHead.load(cascadeNameHead);
	cascadeBrake.load(cascadeNameBrake);

	cvCapture.open(0);
	if (!cvCapture.isOpened()) {
		std::cerr << "Could not open video" << std::endl;
	}
}

// destructor
Tracker::~Tracker()
{
}

// update the tracking
void Tracker::update() {
	//We get the last image from the webcam
	cvCapture >> img;
	detectFace(img, cascadeHead, scale);
#if TRACK_BRAKE==1
	detectBrake(img, cascadeBrake, scale);
#endif
			
}

// perform the face detection
void Tracker::detectFace(cv::Mat& img, cv::CascadeClassifier& cascade, double scale) {
	int i = 0;
	double t = 0;
	std::vector<cv::Rect> faces;

	cv::Mat gray, smallImg(cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1);

	// get the small image
	cv::cvtColor(img, gray, CV_BGR2GRAY);
	cv::resize(gray, smallImg, smallImg.size(), 0, 0, cv::INTER_LINEAR);
	cv::equalizeHist(smallImg, smallImg);

	// detect faces using the haar feature-based classifier
	cascade.detectMultiScale(smallImg, faces,
		1.1, 3, 0
		|CV_HAAR_FIND_BIGGEST_OBJECT
		|CV_HAAR_DO_ROUGH_SEARCH
		|CV_HAAR_DO_CANNY_PRUNING
		|CV_HAAR_SCALE_IMAGE,
		cv::Size(300, 300));

	// iterate through the faces
	for(std::vector<cv::Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++)
	{
		// get the positions
		posXHead = cvRound((r->x + r->width*0.5) * scale);
		posYHead = cvRound((r->y + r->height*0.5) * scale);
		//Depth is computed using the size
		posZHead = cvRound(r->width*r->height*0.5 * scale);
	}    
}

void Tracker::detectBrake(cv::Mat& img, cv::CascadeClassifier& cascade, double scale) {
	int i = 0;
	double t = 0;
	std::vector<cv::Rect> faces;

	cv::Mat gray, smallImg(cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1);

	// get the small image
	cv::cvtColor(img, gray, CV_BGR2GRAY);
	cv::resize(gray, smallImg, smallImg.size(), 0, 0, cv::INTER_LINEAR);
	cv::equalizeHist(smallImg, smallImg);

	// detect brake (fist or QR)
	cascade.detectMultiScale(smallImg, faces,
		1.1, 3, 0
		|CV_HAAR_FIND_BIGGEST_OBJECT
		|CV_HAAR_DO_ROUGH_SEARCH
		|CV_HAAR_DO_CANNY_PRUNING
		|CV_HAAR_SCALE_IMAGE,
		cv::Size(80, 80));

	// iterate through the faces
	for(std::vector<cv::Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++)
	{
		// get the positions
		posXBrake = cvRound((r->x + r->width*0.5)*scale);
		posYBrake = cvRound((r->y + r->height*0.5)*scale);
	}
}
