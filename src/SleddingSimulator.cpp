#include "SleddingSimulator.h"

SleddingSimulator::SleddingSimulator() : mRoot(0)
{
}

SleddingSimulator::~SleddingSimulator()
{
	// OgreBullet physic delete - RigidBodies
	std::deque<OgreBulletDynamics::RigidBody*>::iterator itBody = mBodies.begin();
	while (mBodies.end() != itBody){   
		delete *itBody;
		++itBody;
	}   
	// OgreBullet physic delete - Shapes
	std::deque<OgreBulletCollisions::CollisionShape*>::iterator itShape = mShapes.begin();
	while (mShapes.end() != itShape){   
		delete *itShape;
		++itShape;
	}
	mBodies.clear();
	mShapes.clear();
	delete mWorld->getDebugDrawer();
	mWorld->setDebugDrawer(0);

	if(mInputListener)
		delete mInputListener;
	if(mRoot)
		OGRE_DELETE mRoot;
}

bool SleddingSimulator::start()
{
	loadRessources();
	setUpScene();
	setUpPhysic();
	createScene();
	createFrameListener();

	//Render infinite loop is done here :
	mRoot->startRendering();

	return true;
}

void SleddingSimulator::loadRessources()
{
	mRoot = new Ogre::Root("./resources/plugins.cfg");
	
	Ogre::ConfigFile configFile;
	configFile.load("./resources/resources.cfg");
	Ogre::ConfigFile::SectionIterator seci = configFile.getSectionIterator();
	
	Ogre::String secName, typeName, archName;
	while (seci.hasMoreElements())
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			typeName = i->first;
			archName = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
			archName, typeName, secName);
		}
	}
}
void SleddingSimulator::setUpScene()
{
	Ogre::RenderSystem* rs = mRoot->getRenderSystemByName("OpenGL Rendering Subsystem");
	mRoot->setRenderSystem(rs);
	rs->setConfigOption("Full Screen", "Yes");
	rs->setConfigOption("Video Mode", "1366 x 768 @ 32-bit colour");
	rs->setConfigOption("VSync", "Yes");
	
	
	mWindow = mRoot->initialise(true, "Sledding Simulator");
	
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	
	
	mSceneMgr = mRoot->createSceneManager("DefaultSceneManager", "Sledding Simulator");
	mSceneMgr->setAmbientLight(Ogre::ColourValue(1.0f, 1.0f, 1.0f));
	
	
	mCamera = mSceneMgr->createCamera("PlayerCam");
	mCamera->setPosition(Ogre::Vector3(100,100,100));
	mCamera->lookAt(Ogre::Vector3(0,0,0));
	mCamera->setNearClipDistance(1);
	
	Ogre::Viewport* vp = mWindow->addViewport(mCamera);
	vp->setBackgroundColour(Ogre::ColourValue(0,0,0));
	mCamera->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));

	Ogre::CompositorManager::getSingleton().addCompositor(vp, "QMSBlur");
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(vp, "QMSBlur", true);

}

void SleddingSimulator::setUpPhysic()
{
	//Set up physic
	mNumEntitiesInstanced = 0;
	Ogre::Vector3 gravity = Ogre::Vector3(0, -9.81, 0);
	Ogre::AxisAlignedBox bounds = Ogre::AxisAlignedBox(Ogre::Vector3(-10000, -10000, -10000), Ogre::Vector3(10000,  10000,  10000));
	mWorld = new OgreBulletDynamics::DynamicsWorld(mSceneMgr, bounds, gravity);
	
	debugDrawer = new OgreBulletCollisions::DebugDrawer();
	mWorld->setDebugDrawer(debugDrawer);
	mWorld->setShowDebugShapes(false);

	Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode("debugDrawer", Ogre::Vector3::ZERO);
	node->attachObject(static_cast <Ogre::SimpleRenderable*>(debugDrawer));
}

void SleddingSimulator::createScene()
{
	Ogre::Light* light = mSceneMgr->createLight("lumiere1");
	light->setDiffuseColour(1.0, 0.7, 1.0);
	light->setSpecularColour(1.0, 0.7, 1.0);
	light->setPosition(20, 20, 20);

	//Create the Sled
	mSled= new Sled(mSceneMgr, mCamera, mWorld, mNumEntitiesInstanced);
	//Create the terrain
	mTerrain = new Terrain(mSceneMgr, mWorld, mNumEntitiesInstanced, 0.1f, 0.8f);
}

void SleddingSimulator::createFrameListener()
{
	mInputListener = new InputListener(mWindow, mCamera, mWorld, mSled);
	mInputListener->showDebugOverlay(true);
	mRoot->addFrameListener(mInputListener);
}
