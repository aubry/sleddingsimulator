#include "TrackerAruco.h"
#include "Parameters.h"

#include <iostream>
#include <cstdio>

// constructor
TrackerAruco::TrackerAruco(Ogre::Quaternion newOrientation) : mNewOrientation(newOrientation) {
	// set cascade xml
	cascadeNameHead = "./haar/haarcascade_frontalface_alt.xml";
	
	// position
	posXHead = 0;
	posYHead = 0;
	posZHead = 0;
	posXBrake = 0;
	posYBrake = 0;
	
	// scale
	scale = 1;
	// try to load the cascade
	cascadeHead.load( cascadeNameHead );
	// capture from camera

	cvCapture.open(0);
	if (!cvCapture.isOpened()) {
		std::cerr << "Could not open video" << endl;
	}
}

// destructor
TrackerAruco::~TrackerAruco()
{
}

// update the tracking
void TrackerAruco::update() {
	cvCapture >> img;
	detectFace(img, cascadeHead, scale);
#if TRACK_BRAKE==1
	detectMarkers(img);		
#endif
}

// perform the face detection
void TrackerAruco::detectFace(cv::Mat& img, cv::CascadeClassifier& cascade, double scale) {
	int i = 0;
	double t = 0;
	std::vector<cv::Rect> faces;

	cv::Mat gray, smallImg(cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1);

	// get the small image
	cv::cvtColor(img, gray, CV_BGR2GRAY);
	cv::resize(gray, smallImg, smallImg.size(), 0, 0, cv::INTER_LINEAR);
	cv::equalizeHist(smallImg, smallImg);

	// detect faces
	cascade.detectMultiScale(smallImg, faces,
		1.1, 3, 0
		|CV_HAAR_FIND_BIGGEST_OBJECT
		|CV_HAAR_DO_ROUGH_SEARCH
		|CV_HAAR_DO_CANNY_PRUNING
		|CV_HAAR_SCALE_IMAGE,
		cv::Size(300, 300));

	// iterate through the faces
	for(vector<cv::Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++)
	{
		// get the positions
		posXHead = cvRound((r->x + r->width*0.5)*scale);
		posYHead = cvRound((r->y + r->height*0.5)*scale);
		posZHead = cvRound(r->width*r->height*0.5 * scale);
	}    
}

// Detect the markers with ArUCo
void TrackerAruco::detectMarkers (cv::Mat& img) {
	mDetector.detect(img, mMarkers);
	for(unsigned int i = 0; i < mMarkers.size(); i++)
	{
		if (firstDetection)
		{
			mInitialMarkerPosition.y = Ogre::Math::DegreesToRadians(mMarkers[i].getCenter().y);
			mInitialAngle = Ogre::Math::DegreesToRadians(mMarkers[i].getCenter().y) - mNewOrientation.x;
			firstDetection = false;
		}

		mNewOrientation.x = bound(Ogre::Math::DegreesToRadians(mMarkers[i].getCenter().y), mInitialMarkerPosition.y) - mInitialAngle;
	}
}

// Bounds the difference between the origin position (of first marker detection)
// and the current position, so that the brake stays between a certain angle range.
Ogre::Real TrackerAruco::bound(Ogre::Real currentPos, Ogre::Real initialPos) {
	Ogre::Real delta = currentPos - initialPos;
	if (delta < -Ogre::Math::TWO_PI/12) delta = -Ogre::Math::TWO_PI/12;
	else if (delta > 0) delta = 0;
	return initialPos + delta;
}