#ifndef _OGREBULLET_Sled__H
#define _OGREBULLET_Sled__H

#include "OgreBulletDynamics.h"

// -------------------------------------------------------------------------
class Sled
{
public:

	Sled(Ogre::SceneManager* sMgr, Ogre::Camera* cam, OgreBulletDynamics::DynamicsWorld* world, int numEnt);
    ~Sled(){};
    
    void changeSteeringLeft(bool steeringLeft);
    void changeSteeringRight(bool steeringRight);
    void changePushForce(int pushForce);
    void updateSled(Ogre::Real elapsedTime);
    bool isTurningLeft();
    bool isTurningRight();
    int  getPushForce();
    void throwBall();
    void brake(float brakingCoefficient);
    Ogre::SceneNode* brakeNode;

    static bool HandleContacts(btManifoldPoint& point, btCollisionObject* body0, btCollisionObject* body1);
    


private:
    Ogre::SceneManager* mSceneMgr;
    int mNumEntitiesInstanced;
    OgreBulletDynamics::DynamicsWorld* mWorld;
    Ogre::Camera* mCamera;

    OgreBulletDynamics::WheeledRigidBody* mSledChassis;
    OgreBulletDynamics::VehicleTuning* mTuning;
    OgreBulletDynamics::VehicleRayCaster* mVehicleRayCaster;
    OgreBulletDynamics::RaycastVehicle* mVehicle;

    Ogre::Entity* mChassis;
    Ogre::Entity* mSkates[4];
    Ogre::SceneNode* mSkateNodes[4];

    int mSkatesEngine[4];
    int mSkatesEngineCount;
    int mSkatesSteerable[4];
    int mSkatesSteerableCount;

    float mPushForce;
    float mSteering;

    bool mSteeringLeft;
    bool mSteeringRight;

    std::deque<OgreBulletDynamics::RigidBody*> mBodies;
    std::deque<OgreBulletCollisions::CollisionShape*> mShapes;
};

#endif