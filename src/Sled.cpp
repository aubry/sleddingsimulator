#include "Sled.h" 

#include "Shapes/OgreBulletCollisionsBoxShape.h"
#include "Shapes/OgreBulletCollisionsSphereShape.h"
#include "Shapes/OgreBulletCollisionsCompoundShape.h"

#include "OgreBulletDynamicsWorld.h"
#include "OgreBulletDynamicsRigidBody.h"
#include "Debug/OgreBulletCollisionsDebugDrawer.h"

#include "Constraints/OgreBulletDynamicsRaycastVehicle.h"

#include "Parameters.h"

// Parameters of the sled (adapted from the ones of a wheeled vehicle, suspension compensate for the softness of the snow)
static float gMaxPushForce = 1000.f;

static float gSteeringIncrement = 0.002f;
static float gSteeringClamp = 0.04f;

static float gSkateRadius = 0.5f;
static float gSkateWidth = 0.7f;

static float gSkatePosX = 0.6f;
static float gSkatePosY = 0.7f;

static float gSkateFriction = 1e30f;
static float gSuspensionStiffness = 20.f;
static float gSuspensionDamping = 2.3f;
static float gSuspensionCompression = 4.4f;

static float gRollInfluence = 0.1f;
static float gSuspensionRestLength = 0.6;
static float gMaxSuspensionTravelCm = 500.0;
static float gFrictionSlip = 10.5;
//initial position
static const Ogre::Vector3 SledPosition = Ogre::Vector3(-15, 15,-15);

#define CUBE_HALF_EXTENTS 1

// -------------------------------------------------------------------------
Sled::Sled(Ogre::SceneManager* sMgr, Ogre::Camera* cam, OgreBulletDynamics::DynamicsWorld* world, int numEnt) : mSceneMgr(sMgr), mCamera(cam), mWorld(world), mNumEntitiesInstanced(numEnt)
{

	// Initialisation
	for (int i = 0; i < 4; i++)
	{
		mSkatesEngine[i] = 0;
		mSkatesSteerable[i] = 0;
	}
	mSkatesEngineCount = 2;
	mSkatesEngine[0] = 0;
	mSkatesEngine[1] = 1;
	mSkatesEngine[2] = 2;
	mSkatesEngine[3] = 3;

	mSkatesSteerableCount = 2;
	mSkatesSteerable[0] = 0;
	mSkatesSteerable[1] = 1;

	mSteeringLeft = false;
	mSteeringRight = false;

	mPushForce = 0;
	mSteering = 0;

	/// create the sled
	{
		//Position of the chassis of the sled (higher than the floor because of the skates)
		const Ogre::Vector3 chassisShift(0, 0.9f, 0);
		//Height of the connection with skates, do not touch, it corresponds to the skates model size.
		float connectionHeight = 0.8f;

		mChassis = mSceneMgr->createEntity(
				"chassis" + Ogre::StringConverter::toString(mNumEntitiesInstanced++),
				"Sled.mesh");

		Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode();

		Ogre::SceneNode* chassisnode = node->createChildSceneNode();
		chassisnode->attachObject(mChassis);
		//chassisShift.y-0.9 almost corresponds to the ground level 
		//(since we cheated on the center of mass, we still have to display the model at the right place)
		chassisnode->setPosition(chassisShift + Ogre::Vector3(0,-0.8f,0.5f));
		//We attach the camera to the sled (1st person view)
#if OFF_AXIS_PROJECTION == 1 && CAM == 1
		mCamera->setPosition(chassisShift + Ogre::Vector3(-0.8,0.7,-1.1));
#else
		mCamera->setPosition(chassisShift + Ogre::Vector3(0,-0.1,-1.1));
#endif
		//Look in front of the sled
		mCamera->lookAt(chassisShift + Ogre::Vector3(0,0,10));
		chassisnode->attachObject(mCamera);

		//Create the brake lever and attach it to the sled
		brakeNode = node->createChildSceneNode();
		brakeNode->attachObject(mSceneMgr->createEntity("Brake", "Brake.mesh"));
		brakeNode->setPosition(Ogre::Vector3(0, 0.3, 1.2));
		brakeNode->setOrientation(Ogre::Quaternion(Ogre::Radian(0.78539816339), Ogre::Vector3::UNIT_X)); // Note : this is π/4

		mChassis->setCastShadows(true);

		//Create a bouding box for the sled, we adapt the size since 
		//The size of an ogre model do not fit the one of bullet
		Ogre::Vector3 size = Ogre::Vector3::ZERO; 
		Ogre::AxisAlignedBox boundingB = mChassis->getBoundingBox();
		size = boundingB.getSize();
		size /= 2.0f; // only the half needed
		size *= 0.96f;

		//The size of the collision shape has a higher center of gravity to improve the stability of the sled
		//It is a bit bigger than the actual size to move the gravity center on the back (on a sled, people usually sit on the back)
		OgreBulletCollisions::BoxCollisionShape* chassisShape = new OgreBulletCollisions::BoxCollisionShape(size+Ogre::Vector3(0.f,0.2f,0.5f));
		OgreBulletCollisions::CompoundCollisionShape* compound = new OgreBulletCollisions::CompoundCollisionShape();
		compound->addChildShape(chassisShape, chassisShift); 

		//Actually create the rigid Body
		mSledChassis = new OgreBulletDynamics::WheeledRigidBody("sledChassis", mWorld);
		//Attach a shape
		mSledChassis->setShape(node, compound, 0.6, 0.6, 600, SledPosition, Ogre::Quaternion(Ogre::Radian(3.14/2), Ogre::Vector3(0,1,0)));
		mSledChassis->setDamping(0.2, 0.2);

		//Set parameters of the sled Vehicle
		mSledChassis->disableDeactivation ();
		mTuning = new OgreBulletDynamics::VehicleTuning(
			gSuspensionStiffness,
			gSuspensionCompression,
			gSuspensionDamping,
			gMaxSuspensionTravelCm,
			gFrictionSlip);

		//Set up raycasting (detection of collisions)
		mVehicleRayCaster = new OgreBulletDynamics::VehicleRayCaster(mWorld);
		mVehicle = new OgreBulletDynamics::RaycastVehicle(mSledChassis, mTuning, mVehicleRayCaster);

		{
			int rightIndex = 0;
			int upIndex = 1;
			int forwardIndex = 2;

			mVehicle->setCoordinateSystem(rightIndex, upIndex, forwardIndex);

			//Create the skates of the sled !

			Ogre::Vector3 skateDirectionCS0(0,-1,0);
			Ogre::Vector3 skateAxleCS(-1,0,0);

			for (size_t i = 0; i < 4; i++)
			{
				mSkates[i] = mSceneMgr->createEntity(
					"skate" + Ogre::StringConverter::toString(mNumEntitiesInstanced++),
					"Skate.mesh");

				//mSkates[i]->setQueryFlags (GEOMETRY_QUERY_MASK);
				mSkates[i]->setCastShadows(true);

				mSkateNodes[i] = mSceneMgr->getRootSceneNode ()->createChildSceneNode ();
				mSkateNodes[i]->attachObject (mSkates[i]);
				mSkateNodes[i]->setVisible(false);
			}

			{
				bool isFrontSkate = true;

				Ogre::Vector3 connectionPointCS0(
					gSkatePosX,
					connectionHeight,
					gSkatePosY);

				// We trick bullet to use skates instead of wheels

				mVehicle->addWheel(
					mSkateNodes[0],
					connectionPointCS0,
					skateDirectionCS0,
					skateAxleCS,
					gSuspensionRestLength,
					gSkateRadius,
					isFrontSkate, gSkateFriction, gRollInfluence);

				connectionPointCS0 = Ogre::Vector3(
					-gSkatePosX,
					connectionHeight,
					gSkatePosY);


				mVehicle->addWheel(
					mSkateNodes[1],
					connectionPointCS0,
					skateDirectionCS0,
					skateAxleCS,
					gSuspensionRestLength,
					gSkateRadius,
					isFrontSkate, gSkateFriction, gRollInfluence);


				connectionPointCS0 = Ogre::Vector3(
					-gSkatePosX,
					connectionHeight,
					-gSkatePosY);

				isFrontSkate = false;
				mVehicle->addWheel(
					mSkateNodes[2],
					connectionPointCS0,
					skateDirectionCS0,
					skateAxleCS,
					gSuspensionRestLength,
					gSkateRadius,
					isFrontSkate, gSkateFriction, gRollInfluence);

				connectionPointCS0 = Ogre::Vector3(
					gSkatePosX,
					connectionHeight,
					-gSkatePosY);

				mVehicle->addWheel(
					mSkateNodes[3],
					connectionPointCS0,
					skateDirectionCS0,
					skateAxleCS,
					gSuspensionRestLength,
					gSkateRadius,
					isFrontSkate, gSkateFriction, gRollInfluence);

			}

		}
	}

}

void Sled::changeSteeringLeft(bool steeringLeft) {
	mSteeringLeft = steeringLeft;
}

void Sled::changeSteeringRight(bool steeringRight) {
	mSteeringRight = steeringRight;
}

void Sled::changePushForce(int pushForce) {
	switch(pushForce) {
		case(1) : mPushForce = gMaxPushForce; break;
		case(-1) : mPushForce = -gMaxPushForce; break;
		default : mPushForce = 0;
	}
}

void Sled::brake(float brakingCoefficient) {
	mPushForce = brakingCoefficient * gMaxPushForce;
}

bool Sled::isTurningLeft() {
	return mSteeringLeft;
}

bool Sled::isTurningRight() {
	return mSteeringRight;
}

int Sled::getPushForce() {
	return mPushForce;
}

//Modify the parameters of the sled (called in the main loop) steering, push force ...
void Sled::updateSled(Ogre::Real elapsedTime)
{

	// apply engine Force on relevant skates
	for (int i = mSkatesEngine[0]; i < mSkatesEngineCount; i++)
	{
		mVehicle->applyEngineForce(mPushForce, mSkatesEngine[i]);
	}

	if (mSteeringLeft)
	{
		if (mSteering < gSteeringClamp)
			mSteering += gSteeringIncrement;
		else if (mSteering > gSteeringClamp)
			mSteering = gSteeringClamp;
	}
	else if (!mSteeringLeft)
	{
		if (mSteering > gSteeringIncrement)
			mSteering -= gSteeringIncrement;
		else if (mSteering > 0)
			mSteering = 0;

	}
	
	if (mSteeringRight)
	{
		if (mSteering > -gSteeringClamp)
			mSteering -= gSteeringIncrement;
		if (mSteering < -gSteeringClamp)
			mSteering = -gSteeringClamp;
	}
	else if (!mSteeringRight)
	{
		if (mSteering < -gSteeringIncrement)
			mSteering += gSteeringIncrement;
		else if (mSteering < 0)
			mSteering = 0;

	}

	// apply Steering on relevant skates
	for (int i = mSkatesSteerable[0]; i < mSkatesSteerableCount; i++)
	{
		if (i < 2)
			mVehicle->setSteeringValue(mSteering, mSkatesSteerable[i]);
		else
			mVehicle->setSteeringValue(-mSteering, mSkatesSteerable[i]);
	}

}
//Throw a snowball =P
void Sled::throwBall()
{
	 Ogre::Vector3 size = Ogre::Vector3::ZERO;   // size of the box
	 // starting position of the box
	 Ogre::Vector3 position = (mCamera->getDerivedPosition() + mCamera->getDerivedDirection().normalisedCopy() * 10);
	 // create an ordinary, Ogre mesh with texture
	 Ogre::Entity *entity = mSceneMgr->createEntity(
		   "Ball" + Ogre::StringConverter::toString(mNumEntitiesInstanced),
		   "Ball.mesh");            
	 entity->setCastShadows(true);
	 // we need the bounding box of the box to be able to set the size of the Bullet-box
	 Ogre::AxisAlignedBox boundingB = entity->getBoundingBox();
	 size = boundingB.getSize();
	 size /= 2.0f; // only the half needed
	 size *= 0.96f; // Bullet margin is a bit bigger so we need a smaller size
					// (Bullet 2.76 Physics SDK Manual page 18)
	 //entity->setMaterialName("Examples/BumpyMetal");
	 Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	 node->attachObject(entity);
	 node->scale(0.05f, 0.05f, 0.05f);   // the cube is too big for us
	 size *= 0.05f;                  // don't forget to scale down the Bullet-box too
	 // after that create the Bullet shape with the calculated size
	 OgreBulletCollisions::SphereCollisionShape* sceneSphereShape = new OgreBulletCollisions::SphereCollisionShape(size[0]);
	 // and the Bullet rigid body
	 OgreBulletDynamics::RigidBody *defaultBody = new OgreBulletDynamics::RigidBody(
		   "defaultSphereRigid" + Ogre::StringConverter::toString(mNumEntitiesInstanced),
		   mWorld);
	 defaultBody->setShape(	node,
							sceneSphereShape,
							0.6f,       // dynamic body restitution
							0.6f,       // dynamic body friction
							0.10f,      // dynamic bodymass
							position,   // starting position of the box
							Ogre::Quaternion(0,0,0,1)); // orientation of the box
	 mNumEntitiesInstanced++;            
	 defaultBody->setLinearVelocity(mCamera->getDerivedDirection().normalisedCopy() * 50.0f); // shooting speed
		   // push the created objects to the deques

	mShapes.push_back(sceneSphereShape);
	mBodies.push_back(defaultBody);            
}
