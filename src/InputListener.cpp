#include "InputListener.h"
#include "Parameters.h"

/* Inspired from ExampleFrameListener from OGRE SDK */
InputListener::InputListener(Ogre::RenderWindow* win, Ogre::Camera* cam, OgreBulletDynamics::DynamicsWorld* wrld, Sled* sled, bool bufferedKeys, bool bufferedMouse, bool bufferedJoy) :
		mCamera(cam), mWorld(wrld), mSled(sled), mTranslateVector(Ogre::Vector3::ZERO), mCurrentSpeed(0), mWindow(win),mMoveScale(0.0f), mRotScale(0.0f), mTimeUntilNextToggle(0),
		mMoveSpeed(100), mRotateSpeed(36), mInputManager(0), mMouse(0), mKeyboard(0), mJoy(0), mDebugOverlay(0), motionBlurIsEnabled(true) {

	Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
	
	OIS::ParamList pl;
	size_t windowHnd = 0;
	std::ostringstream windowHndStr;
	
	mWindow->getCustomAttribute("WINDOW", &windowHnd);
	windowHndStr << windowHnd;
	pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
	
	mInputManager = OIS::InputManager::createInputSystem(pl);
	
	mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, bufferedKeys));
	mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, bufferedMouse));
	try {
		mJoy = static_cast<OIS::JoyStick*>(mInputManager->createInputObject(OIS::OISJoyStick, bufferedJoy));
	}
	catch (...) {
		mJoy = 0;
	}

	windowResized(mWindow);

	showDebugOverlay(false);

	Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);
	
	// Populate the camera and scene manager containers
	mCamera = cam;

#if CAM == 1
		// head vectors
		headPos = Ogre::Vector3::ZERO;
		headDelta = Ogre::Vector3::ZERO;
		lastHeadPos = Ogre::Vector3::ZERO;

		// frame counter
		headCount = 0;

		mBrakeNode = mSled->brakeNode;

		// head tracking webcam
#if ARUCO == 1
		tracker = new TrackerAruco(mBrakeNode->getOrientation());
#else
		tracker = new Tracker();
#endif

		previousMatrix = mCamera->getProjectionMatrix();

#endif

}

void InputListener::showDebugOverlay(bool show)
{
	if (mDebugOverlay)
	{
		if (show)
			mDebugOverlay->show();
		else
			mDebugOverlay->hide();
	}
}

InputListener::~InputListener()
{
	Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
	windowClosed(mWindow);
}


bool InputListener::processUnbufferedKeyInput(const Ogre::FrameEvent& evt)
{
// If the cam is disabled, use keyboard/mouse commands
#if CAM == 0

	Ogre::Real moveScale = mMoveScale;
	if(mKeyboard->isKeyDown(OIS::KC_LSHIFT))
		moveScale *= 10;

	if(mKeyboard->isKeyDown(OIS::KC_A))
		mTranslateVector.x = -moveScale;  // Move camera left

	if(mKeyboard->isKeyDown(OIS::KC_D))
		mTranslateVector.x = moveScale; // Move camera RIGHT

	if(mKeyboard->isKeyDown(OIS::KC_UP) || mKeyboard->isKeyDown(OIS::KC_W) )
		mTranslateVector.z = -moveScale;  // Move camera forward

	if(mKeyboard->isKeyDown(OIS::KC_DOWN) || mKeyboard->isKeyDown(OIS::KC_S) )
		mTranslateVector.z = moveScale; // Move camera backward

	if(mKeyboard->isKeyDown(OIS::KC_PGUP))
		mTranslateVector.y = moveScale; // Move camera up

	if(mKeyboard->isKeyDown(OIS::KC_PGDOWN))
		mTranslateVector.y = -moveScale;  // Move camera down

	if(mKeyboard->isKeyDown(OIS::KC_RIGHT))
		mCamera->yaw(-mRotScale);

	if(mKeyboard->isKeyDown(OIS::KC_LEFT))
		mCamera->yaw(mRotScale);

	bool isTurningLeft = mSled->isTurningLeft();
	bool isTurningRight = mSled->isTurningRight();
	bool J_state = mKeyboard->isKeyDown(OIS::KC_J);
	bool L_state = mKeyboard->isKeyDown(OIS::KC_L);
	if(J_state && !isTurningLeft)
		mSled->changeSteeringLeft(true);
	if(!J_state && isTurningLeft)
		mSled->changeSteeringLeft(false);

	if(L_state && !isTurningRight)
		mSled->changeSteeringRight(true);
	if(!L_state && isTurningRight)
		mSled->changeSteeringRight(false);

#endif

#if TRACK_BRAKE == 0 || CAM == 0

	bool I_state = mKeyboard->isKeyDown(OIS::KC_I);
	bool K_state = mKeyboard->isKeyDown(OIS::KC_K);

	int pushForce = mSled->getPushForce();

	if(I_state && pushForce<=0)
		mSled->changePushForce(1);
	if(!I_state && pushForce>0)
		mSled->changePushForce(0);

	if(K_state && pushForce>=0)
		mSled->changePushForce(-1);
	if(!K_state && pushForce<0)
		mSled->changePushForce(0);

#endif

	if( mKeyboard->isKeyDown(OIS::KC_ESCAPE) || mKeyboard->isKeyDown(OIS::KC_Q))
		return false;
	//Easter egg ! Let's throw snowballs !
	if(mKeyboard->isKeyDown(OIS::KC_B) && mTimeUntilNextToggle <= 0) {
			mSled->throwBall();
			mTimeUntilNextToggle = 0.5;
	}

	if(mKeyboard->isKeyDown(OIS::KC_M)) {  
		printf("motionBlurIsEnabled: %d\n", motionBlurIsEnabled); 
		motionBlurIsEnabled = !motionBlurIsEnabled;  
		Ogre::CompositorManager::getSingleton().setCompositorEnabled(mWindow->getViewport(0), "QMSBlur", motionBlurIsEnabled);
	}

	// Return true to continue rendering
	return true;
}

bool InputListener::processUnbufferedMouseInput(const Ogre::FrameEvent& evt)
{

#if CAM == 0
	// Rotation factors, may not be used if the second mouse button is pressed
	// 2nd mouse button - slide, otherwise rotate
	const OIS::MouseState &ms = mMouse->getMouseState();
	if(ms.buttonDown(OIS::MB_Right))
	{
		mTranslateVector.x += ms.X.rel * 0.13;
		mTranslateVector.y -= ms.Y.rel * 0.13;
	}
	else
	{
		mRotX = Ogre::Degree(-ms.X.rel * 0.13);
		mRotY = Ogre::Degree(-ms.Y.rel * 0.13);
	}

#endif

	return true;
}

void InputListener::moveCamera()
{

#if CAM == 0
	// Make all the changes to the camera
	// Note that YAW direction is around a fixed axis (freelook style) rather than a natural YAW
	//(e.g. airplane)
	mCamera->yaw(mRotX);
	mCamera->pitch(mRotY);
	mCamera->moveRelative(mTranslateVector);
#endif
}

// This method is called at every loop, while the GPU is computing the Scene
bool InputListener::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	if(mWindow->isClosed()) return false;

#if CAM == 0

	mSpeedLimit = mMoveScale * evt.timeSinceLastFrame;

	//Need to capture/update each device
	mKeyboard->capture();
	mMouse->capture();
	if(mJoy) mJoy->capture();

	bool buffJ = (mJoy) ? mJoy->buffered() : true;

	Ogre::Vector3 lastMotion = mTranslateVector;

	//Check if one of the devices is not buffered
	if(!mMouse->buffered() || !mKeyboard->buffered() || !buffJ)
	{
		// one of the input modes is immediate, so setup what is needed for immediate movement
		if (mTimeUntilNextToggle >= 0)
			mTimeUntilNextToggle -= evt.timeSinceLastFrame;

		// Move about 100 units per second
		mMoveScale = mMoveSpeed * evt.timeSinceLastFrame;
		// Take about 10 seconds for full rotation
		mRotScale = mRotateSpeed * evt.timeSinceLastFrame;

		mRotX = 0;
		mRotY = 0;
		mTranslateVector = Ogre::Vector3::ZERO;

	}

	//Check to see which device is not buffered, and handle it
	if(!mKeyboard->buffered())
		if(processUnbufferedKeyInput(evt) == false)
			return false;
	if(!mMouse->buffered())
		if(processUnbufferedMouseInput(evt) == false)
			return false;

	// ramp up / ramp down speed
	if (mTranslateVector == Ogre::Vector3::ZERO)
	{
		// decay (one third speed)
		mCurrentSpeed -= evt.timeSinceLastFrame * 0.3;
		mTranslateVector = lastMotion;
	}
	else
	{
		// ramp up
		mCurrentSpeed += evt.timeSinceLastFrame;

	}
	// Limit motion speed
	if (mCurrentSpeed > 1.0)
		mCurrentSpeed = 1.0;
	if (mCurrentSpeed < 0.0)
		mCurrentSpeed = 0.0;

	mTranslateVector *= mCurrentSpeed;


	if(!mMouse->buffered() || !mKeyboard->buffered() || !buffJ)
		moveCamera();
#else
	mKeyboard->capture();
	mMouse->capture();
	//Check if one of the devices is not buffered
	if(!mKeyboard->buffered())
	{
		// one of the input modes is immediate, so setup what is needed for immediate movement
		if (mTimeUntilNextToggle >= 0)
			mTimeUntilNextToggle -= evt.timeSinceLastFrame;

		if(processUnbufferedKeyInput(evt) == false)
			return false;

	}

#endif

	return true;
}


void InputListener::windowResized(Ogre::RenderWindow* wnd)
{
	unsigned int width, height, depth;
	int left, top;
	wnd->getMetrics(width, height, depth, left, top);

	const OIS::MouseState &ms = mMouse->getMouseState();
	ms.width = width;
	ms.height = height;
}

void InputListener::windowClosed(Ogre::RenderWindow* wnd)
{
	if(wnd == mWindow)
	{
		if(mInputManager)
		{
			mInputManager->destroyInputObject(mMouse);
			mInputManager->destroyInputObject(mKeyboard);

			OIS::InputManager::destroyInputSystem(mInputManager);
			mInputManager = 0;
		}
	}
}

//this method is called at the beginning of the render loop
bool InputListener::frameStarted(const Ogre::FrameEvent& evt)
{
#if CAM == 1
	//Every 3 frames, we track what we have to track (face, marker, fist ...)
	if(headCount%3 == 0)
		tracker->update();
	
	headCount++;

	// update positions
	headPos.x = (float(tracker->posXHead)-160)/160 * 0.25;
	headPos.y = (float(tracker->posYHead)-120)/120 * 0.25;
	headPos.z = (float(tracker->posZHead)-40000)/60000 *0.5;

	brakePos.x = (float(tracker->posXBrake)-160)/160 * 0.25;
	brakePos.y = (float(tracker->posYBrake)-120)/120 * 0.25;

#if OFF_AXIS_PROJECTION == 1
	headDelta = lastHeadPos - headPos;

	// near plane
	float zNear = .5;
	
	//custom matrix for x y
	Ogre::Matrix4 headMatrix = Ogre::Matrix4( 
	1, 0, headDelta.x, headDelta.x*zNear,
	0, 1, headDelta.y, headDelta.y*zNear,
	0, 0, 1, 0,
	0, 0, 0, 1);

	// custom matrix for z (fov)
	float scaleFov = 1 + headPos.z/40;
	Ogre::Matrix4 fovMatrix = Ogre::Matrix4( 
	scaleFov, 0, 0, 0,
	0, scaleFov, 0, 0, 
	0, 0, 1, 0,
	0, 0, 0, 1);

	// get previous matrix
	Ogre::Matrix4 currentMatrix = headMatrix * previousMatrix;
	// set projection matrix
	mCamera->setCustomProjectionMatrix(true, currentMatrix*fovMatrix);
	mCamera->moveRelative(Ogre::Vector3(headDelta.x*3.0, headDelta.y*3.0, headDelta.z*3.0));

	// keep last position
	lastHeadPos = headPos;

	// keep last projection matrix (without keeping the fovMatrix)
	previousMatrix = currentMatrix;
#endif
	// Adapt the position of the sled according to the position of the head
	if((headPos.x)>0.35)
	{
		mSled->changeSteeringLeft(true);
		mSled->changeSteeringRight(false);
	}
	else if((headPos.x)<0.15)
	{
		mSled->changeSteeringRight(true);
		mSled->changeSteeringLeft(false);
	}
	else
	{
		mSled->changeSteeringRight(false);
		mSled->changeSteeringLeft(false);
	}

#if TRACK_BRAKE == 1
//Manage the brake lever (update display and actually brake)
#if ARUCO==1
	mBrakeNode->setOrientation(tracker->mNewOrientation);
	mSled->brake(tracker->mNewOrientation.x/(Ogre::Math::TWO_PI/12) - 0.3);
#else
	float position = brakePos.y;
	if (brakePos.y < 0) position = 0.6;
	if (brakePos.y < 0.3) position = 0.3;
	mBrakeNode->setOrientation(Ogre::Quaternion(Ogre::Radian(0.78539816339)+Ogre::Radian((position-0.6)*5), Ogre::Vector3::UNIT_X));
	mSled->brake((position-0.5)*3);
#endif

#endif

#endif

	mSled->updateSled(evt.timeSinceLastFrame);
	
	//Update Bullet Physics animation
	mWorld->stepSimulation(evt.timeSinceLastFrame);
	return true;
}

//This method is called at the end of the render loop
bool InputListener::frameEnded(const Ogre::FrameEvent& evt)
{
	//Update Bullet Physics animation
	mWorld->stepSimulation(evt.timeSinceLastFrame);
	return true;
}
